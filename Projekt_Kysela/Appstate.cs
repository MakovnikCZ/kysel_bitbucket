﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Media.Capture;
using Windows.Storage;

namespace Projekt_Kysela
{
    class Appstate
    {
        private StorageFolder cilovaSlozka;
        private int vyska;
        private int sirka;
        //výchozí hodnota
        private CameraCaptureUIPhotoFormat format;
        private static Appstate instance = null;

        //výchozí hodnoty
        private Appstate()
        {
            vyska = 1280;
            sirka = 800;
            format = CameraCaptureUIPhotoFormat.Jpeg;
            cilovaSlozka = KnownFolders.PicturesLibrary;
        }

        public static Appstate Instance
        {
            get
            {
                if (instance == null)
                {
                    return instance = new Appstate();
                }
                return instance;
            }
        }



        public StorageFolder CilovaSlozka
        {
            set { cilovaSlozka = value; }
            get { return cilovaSlozka; }
        }

        public int Sirka
        {
            set { sirka = value; }
            get { return sirka; }
        }

        public int Vyska
        {
            set { vyska = value; }
            get { return vyska; }
        }

        public CameraCaptureUIPhotoFormat Format
        {
            set { format = value; }
            get { return format; }
        }

    }
}
