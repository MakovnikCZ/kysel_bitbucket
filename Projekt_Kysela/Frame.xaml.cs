﻿using System;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.Media.Capture;
using Windows.Storage;
using Windows.UI.ViewManagement;
using Windows.Graphics.Display;
using Windows.Storage.Pickers;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.Graphics.Imaging;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Media.Ocr;
using System.Collections.Generic;
using Windows.Storage.FileProperties;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Input;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Projekt_Kysela
{

    public sealed partial class FramePreview : Page
    {
        PointerPoint Point1, Point2;
        SoftwareBitmap preview;
        private Appstate nastaveni = Appstate.Instance;
        private TranslateTransform dragTranslation;
        private CompositeTransform deltaTransform;
        private TransformGroup rectangleTransforms;

        public FramePreview()
        {
            this.InitializeComponent();

            //získám rozlišení ZOBRAZENÍ pro defaultní nastavení velikosti
            var bounds = ApplicationView.GetForCurrentView().VisibleBounds;
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;

         
        }
        
        

        Boolean isDragging = false;

        private void Grid_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            Point1 = e.GetCurrentPoint(resultImage);
            isDragging = true;
        }

        private void Grid_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            
            Point2 = e.GetCurrentPoint(resultImage);
            if (isDragging) { 
                rect.Visibility = Visibility.Visible;
            rect.Width = (int)Math.Abs(Point2.Position.X - Point1.Position.X);
            rect.Height = (int)Math.Abs(Point2.Position.Y - Point1.Position.Y);
            rect.SetValue(Canvas.LeftProperty, (Point1.Position.X < Point2.Position.X) ? Point1.Position.X : Point2.Position.X);
            rect.SetValue(Canvas.TopProperty, (Point1.Position.Y < Point2.Position.Y) ? Point1.Position.Y : Point2.Position.Y);
            }
        }

        private async void Grid_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            Point2 = e.GetCurrentPoint(resultImage);
            rect.Visibility = Visibility.Visible;
            rect.Width = (int)Math.Abs(Point2.Position.X - Point1.Position.X);
            rect.Height = (int)Math.Abs(Point2.Position.Y - Point1.Position.Y);
            rect.SetValue(Canvas.LeftProperty, (Point1.Position.X < Point2.Position.X) ? Point1.Position.X : Point2.Position.X);
            rect.SetValue(Canvas.TopProperty, (Point1.Position.Y < Point2.Position.Y) ? Point1.Position.Y : Point2.Position.Y);
             isDragging = false;
        }


        internal async System.Threading.Tasks.Task SetImageAsync(SoftwareBitmap softwareBitmap)
        {
            preview = softwareBitmap;
            var source = new SoftwareBitmapSource();
            await source.SetBitmapAsync(softwareBitmap);
            resultImage.Source = source;
        }
        private async void ButtonUlozit_Click(object sender, RoutedEventArgs e)
        {
            var savePicker = new Windows.Storage.Pickers.FileSavePicker();
            savePicker.SuggestedStartLocation =
                Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
            // Dropdown of file types the user can save the file as
            savePicker.FileTypeChoices.Add("JPEG", new List<string>() { ".jpeg" });
            savePicker.FileTypeChoices.Add("PNG", new List<string>() { ".png" });
            // Default file name if the user does not type one in or select a file to replace
            savePicker.SuggestedFileName = "Scan_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
              StorageFile file = await savePicker.PickSaveFileAsync();
            if (file != null) { 
            SaveSoftwareBitmapToFile(preview, file);
                BackButton_Click(sender,  e);
            }
        }

        private async Task<SoftwareBitmap> CreateFromBitmap(SoftwareBitmap softwareBitmap, Rect rect)
        {
            using (InMemoryRandomAccessStream stream = new InMemoryRandomAccessStream())
            {
                BitmapEncoder encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.BmpEncoderId, stream);

                encoder.SetSoftwareBitmap(softwareBitmap);

                encoder.BitmapTransform.Bounds = new BitmapBounds()
                {
                    X = (uint)rect.X,
                    Y = (uint)rect.Y,
                    Height = (uint)rect.Height,
                    Width = (uint)rect.Width
                };

                await encoder.FlushAsync();

                BitmapDecoder decoder = await BitmapDecoder.CreateAsync(stream);

                return await decoder.GetSoftwareBitmapAsync(softwareBitmap.BitmapPixelFormat, softwareBitmap.BitmapAlphaMode);
            }
        }

        private async void SaveSoftwareBitmapToFile(SoftwareBitmap softwareBitmap, StorageFile outputFile)
        {
            using (IRandomAccessStream stream = await outputFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                // Create an encoder with the desired format
                BitmapEncoder encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.JpegEncoderId, stream);

                // Set the software bitmap
                encoder.SetSoftwareBitmap(softwareBitmap);

                // Set additional encoding parameters, if needed
                //   encoder.BitmapTransform.ScaledWidth = 320;
                //    encoder.BitmapTransform.ScaledHeight = 240;
                //     encoder.BitmapTransform.Rotation = Windows.Graphics.Imaging.BitmapRotation.Clockwise90Degrees;
                OcrEngine ocrEngine = OcrEngine.TryCreateFromUserProfileLanguages();
                String result;
                if (Point1 != null && Point2 != null)

                {
                    BitmapBounds bounds = new BitmapBounds();
                    bounds.Height = (uint)rect.ActualHeight;
                    bounds.Width = (uint)rect.ActualWidth;
                    bounds.X = (uint)Point1.Position.X;
                    bounds.Y = (uint)Point1.Position.Y;
                    
                    encoder.BitmapTransform.Bounds = bounds;
                    var croppedBitmap = await CreateFromBitmap(softwareBitmap, new Rect(bounds.X, bounds.Y, bounds.Width,bounds.Height));
                  
                    OcrResult ocrResultCrop = await ocrEngine.RecognizeAsync(croppedBitmap);
                    result = ocrResultCrop.Text;
                }
                else
                {
                    OcrResult ocrResult = await ocrEngine.RecognizeAsync(softwareBitmap);
                    result = ocrResult.Text;
                }
                var savePicker = new Windows.Storage.Pickers.FileSavePicker();
                savePicker.SuggestedStartLocation =
                    Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
                // Dropdown of file types the user can save the file as
                savePicker.FileTypeChoices.Add("TEXT", new List<string>() { ".txt" });
                // Default file name if the user does not type one in or select a file to replace
                savePicker.SuggestedFileName = "Scan_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                StorageFile file = await savePicker.PickSaveFileAsync();
                await Windows.Storage.FileIO.WriteTextAsync(file, result);

                Debug.WriteLine(result);
                encoder.BitmapTransform.InterpolationMode = BitmapInterpolationMode.Fant;
                encoder.IsThumbnailGenerated = true;

              


                try
                {
                    await encoder.FlushAsync();
                }
                catch (Exception err)
                {
                    const int WINCODEC_ERR_UNSUPPORTEDOPERATION = unchecked((int)0x88982F81);
                    switch (err.HResult)
                    {
                        case WINCODEC_ERR_UNSUPPORTEDOPERATION:
                            // If the encoder does not support writing a thumbnail, then try again
                            // but disable thumbnail generation.
                            encoder.IsThumbnailGenerated = false;
                            break;
                        default:
                            throw;
                    }
                }

                if (encoder.IsThumbnailGenerated == false)
                {
                    await encoder.FlushAsync();
                }


            }
        }

        //po kliknutí na uložit nastavení zapne preview
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            //otevřu frame s preview
            Frame frame = new Frame();
            Window.Current.Content = frame;
            frame.Navigate(typeof(MainPage));
            Window.Current.Activate();
        }
    }
}
