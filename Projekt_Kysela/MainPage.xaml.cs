﻿using System;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.Media.Capture;
using Windows.Storage;
using Microsoft.Toolkit.Uwp.Helpers;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Graphics.Imaging;
using Windows.Media.MediaProperties;
using Windows.Storage.Streams;
using Windows.Storage.FileProperties;
using Windows.Media.Capture.Frames;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using System.Collections.Generic;

namespace Projekt_Kysela
{

    public sealed partial class MainPage : Page
    {
        private MainPage rootPage;

        private MediaCapture _mediaCapture = null;
        private MediaFrameReader _reader = null;
       // private FrameRenderer _previewRenderer = null;
        private FrameRenderer _outputRenderer = null;

        private int _frameCount = 0;

        private const int IMAGE_ROWS = 800;
        private const int IMAGE_COLS = 1020;

        private DispatcherTimer _FPSTimer = null;

        public MainPage()
        {
            this.InitializeComponent();
             _outputRenderer = new FrameRenderer(cameraPreview);     

            _FPSTimer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromSeconds(1)
            };
            _FPSTimer.Tick += UpdateFPS;
        }

        /// <summary>
        /// Initializes the MediaCapture object with the given source group.
        /// </summary>
        /// <param name="sourceGroup">SourceGroup with which to initialize.</param>
        private async Task InitializeMediaCaptureAsync(MediaFrameSourceGroup sourceGroup)
        {
            if (_mediaCapture != null)
            {
                return;
            }

            _mediaCapture = new MediaCapture();
            var settings = new MediaCaptureInitializationSettings()
            {
                SourceGroup = sourceGroup,
                SharingMode = MediaCaptureSharingMode.ExclusiveControl,
                StreamingCaptureMode = StreamingCaptureMode.Video,
                MemoryPreference = MediaCaptureMemoryPreference.Cpu
            };
            await _mediaCapture.InitializeAsync(settings);
        }

        private async void Scan_Click(object sender, RoutedEventArgs e)
        {
            var savePicker = new Windows.Storage.Pickers.FileSavePicker();
            savePicker.SuggestedStartLocation =
                Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
            // Dropdown of file types the user can save the file as
            savePicker.FileTypeChoices.Add("JPEG", new List<string>() { ".jpeg" });
            savePicker.FileTypeChoices.Add("PNG", new List<string>() { ".png" });
            // Default file name if the user does not type one in or select a file to replace
            savePicker.SuggestedFileName = "Scan_"+DateTime.Now.ToString("ddMMyyyyHHmmss");
        
            using (var captureStream = new InMemoryRandomAccessStream())
            {
                //vytvořím JPEG
                // if (nastaveni.Format == CameraCaptureUIPhotoFormat.Jpeg)
                //{
                // file = await myPictures.CreateFileAsync(DateTime.Now.ToString("ddMMyyyyHHmmss") + ".jpeg", CreationCollisionOption.GenerateUniqueName);
            //  StorageFile file = await savePicker.PickSaveFileAsync();
                await _mediaCapture.CapturePhotoToStreamAsync(ImageEncodingProperties.CreateJpeg(), captureStream);
               // }
               // else //vytvořím PNG
                //{
                   // file = await myPictures.CreateFileAsync(DateTime.Now.ToString("ddMMyyyyHHmmss") + "pokus.png", CreationCollisionOption.GenerateUniqueName);
                 //   await mediaCaptureMgr.CapturePhotoToStreamAsync(ImageEncodingProperties.CreatePng(), captureStream);
               // }
               
               // using (var fileStream = await file.OpenAsync(FileAccessMode.ReadWrite))
               // {
                    var decoder = await BitmapDecoder.CreateAsync(captureStream);
                   //     var encoder = await BitmapEncoder.CreateForTranscodingAsync(fileStream, decoder);
                     SoftwareBitmap   softwareBitmap = await decoder.GetSoftwareBitmapAsync();
                    //přehodím frame na Nastavení
                    FramePreview nastaveni = new FramePreview();
                    await nastaveni.SetImageAsync(SoftwareBitmap.Convert(softwareBitmap, BitmapPixelFormat.Bgra8, BitmapAlphaMode.Premultiplied));
                    Window.Current.Content = nastaveni;
                    Window.Current.Activate();
                    // if (nastaveni.Format == CameraCaptureUIPhotoFormat.Jpeg) //u png nenastavuji properties
                    // {
               //     var properties = new BitmapPropertySet {
                 //            { "System.Photo.Orientation", new BitmapTypedValue(PhotoOrientation.Normal, PropertyType.UInt16) }
                   //     };
                     //   await encoder.BitmapProperties.SetPropertiesAsync(properties);

                   // }
                   // await encoder.FlushAsync();
               // }
            }
        }

        /// <summary>
        /// Unregisters FrameArrived event handlers, stops and disposes frame readers
        /// and disposes the MediaCapture object.
        /// </summary>
        private async Task CleanupMediaCaptureAsync()
        {
            if (_mediaCapture != null)
            {
                await _reader.StopAsync();
                _reader.FrameArrived -= ColorFrameReader_FrameArrivedAsync;
                _reader.Dispose();
                _mediaCapture = null;
            }
        }

        private void UpdateFPS(object sender, object e)
        {
            var frameCount = Interlocked.Exchange(ref _frameCount, 0);
           // this.FPSMonitor.Text = "FPS: " + frameCount;
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            rootPage = this;
            // Find the sources 
            var allGroups = await MediaFrameSourceGroup.FindAllAsync();
            var sourceGroups = allGroups.Select(g => new
            {
                Group = g,
                SourceInfo = g.SourceInfos.FirstOrDefault(i => i.SourceKind == MediaFrameSourceKind.Color)
            }).Where(g => g.SourceInfo != null).ToList();

            if (sourceGroups.Count == 0)
            {
                // No camera sources found
                return;
            }
            var selectedSource = sourceGroups.FirstOrDefault();

            // Initialize MediaCapture
            try
            {
                await InitializeMediaCaptureAsync(selectedSource.Group);
            }
            catch (Exception exception)
            {
                Debug.WriteLine("MediaCapture initialization error: " + exception.Message);
                await CleanupMediaCaptureAsync();
                return;
            }

            // Create the frame reader
            MediaFrameSource frameSource = _mediaCapture.FrameSources[selectedSource.SourceInfo.Id];
            BitmapSize size = new BitmapSize() // Choose a lower resolution to make the image processing more performant
            {
                Height = IMAGE_ROWS,
                Width = IMAGE_COLS
            };
            _reader = await _mediaCapture.CreateFrameReaderAsync(frameSource, MediaEncodingSubtypes.Bgra8, size);
            _reader.FrameArrived += ColorFrameReader_FrameArrivedAsync;
            await _reader.StartAsync();

            _FPSTimer.Start();
        }

        protected override async void OnNavigatedFrom(NavigationEventArgs args)
        {
            _FPSTimer.Stop();
            await CleanupMediaCaptureAsync();
        }

        /// <summary>
        /// Handles a frame arrived event and renders the frame to the screen.
        /// </summary>
        private void ColorFrameReader_FrameArrivedAsync(MediaFrameReader sender, MediaFrameArrivedEventArgs args)
        {
            var frame = sender.TryAcquireLatestFrame();
            if (frame != null)
            {
                SoftwareBitmap originalBitmap = null;
                var inputBitmap = frame.VideoMediaFrame?.SoftwareBitmap;
                if (inputBitmap != null)
                {
                    // The XAML Image control can only display images in BRGA8 format with premultiplied or no alpha
                    // The frame reader as configured in this sample gives BGRA8 with straight alpha, so need to convert it
                    originalBitmap = SoftwareBitmap.Convert(inputBitmap, BitmapPixelFormat.Bgra8, BitmapAlphaMode.Premultiplied);

                    // Display both the original bitmap and the processed bitmap.
                  //  _previewRenderer.RenderFrame(originalBitmap);
                    _outputRenderer.RenderFrame(originalBitmap);

                }

                Interlocked.Increment(ref _frameCount);
            }
        }

     
        private void OtevriNastaveniButton_Click(object sender, RoutedEventArgs e)
        {
            hlavniSplitView.IsPaneOpen = !hlavniSplitView.IsPaneOpen;
        }
    }
}
